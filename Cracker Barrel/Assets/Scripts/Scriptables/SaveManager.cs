﻿using UnityEngine;
using System.Collections;

public class SaveManager : MonoBehaviour
{
    static SaveManager _instance = null;
    public static SaveManager Instance
    {
        get
        {
            return _instance;
        }
    }

    public MoveList moveList;

    void Awake()
    {
        _instance = this;

        //TODO 
        //asset management, instead of dragging moveList reference
        //load from Resource folder or asset bundle
        //or make a new one
    }

    public void RestartMoveList()
    {
        moveList.moves.Clear();
    }

    public void AddMove(MoveData _data)
    {
        moveList.moves.Add(_data);
    }

    public int MoveCount
    {
        get
        {
           return moveList.moves.Count;
        }
    }
}
