﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MoveList : ScriptableObject
{
    public List<MoveData> moves;
    public int amountPegs;
}

[System.Serializable]
public class MoveData {

    public tHole startHole, endHole;
    public tHole deletedPegHole;
}
