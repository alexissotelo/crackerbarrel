﻿using UnityEngine;
using System.Collections;

public class PegSelector : MonoBehaviour
{
    enum PegSelectionState
    {
        highlight, holding
    }
    PegSelectionState pegSelectionState;

    public LayerMask boardLayer;
    public LayerMask pegLayer;

    private PegCtrl lastPeg;

    void Start()
    {
        pegSelectionState = PegSelectionState.highlight;
        lastPeg = null;
    }

    void FixedUpdate()
    {
        switch (pegSelectionState)
        {
            case PegSelectionState.highlight:
                Highlight();
                break;
            case PegSelectionState.holding:
                HoldingPeg();
                break;
        }

    }

    void Highlight()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, pegLayer))
        {
            PegCtrl pegCtrl = hit.transform.gameObject.GetComponent<PegCtrl>();
            if (pegCtrl)
            {
                if (pegCtrl != lastPeg)
                {
                    if (lastPeg != null)
                        lastPeg.EndHightlight();

                    pegCtrl.StartHighlight();
                }

                if (Input.GetMouseButtonDown(0) && pegCtrl.CanHold())
                {
                    pegSelectionState = PegSelectionState.holding;
                    pegCtrl.StartHolding();
                    //return;
                }

                lastPeg = pegCtrl;
            }
        }
        else
        {
            if (lastPeg)
                lastPeg.EndHightlight();
            lastPeg = null;
        }
    }

    void HoldingPeg()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, boardLayer))
        {
            Vector3 pos = hit.point;
            //pos.y = lastPeg.transform.position.y;
            lastPeg.transform.position = pos;
        }

        if (Input.GetMouseButtonDown(0))
        {
            pegSelectionState = PegSelectionState.highlight;

            int _moveIndex = lastPeg.IsValidHole();
            bool validMove = _moveIndex >= 0;
            if (validMove)
            {
                //Apply Move
                lastPeg.EndHightlight();
                lastPeg.MakeMove(_moveIndex);
            }
            else
            {
                lastPeg.EndHightlight();
                lastPeg.ResetHolding();
                lastPeg = null;
            }

        }
    }


}
