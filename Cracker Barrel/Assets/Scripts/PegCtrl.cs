﻿using UnityEngine;
using System.Collections;

public class PegCtrl : MonoBehaviour
{
    public LayerMask holeLayer;

    public Renderer render;
    private Color normalColor;

    private Vector3 startPos;
    private Hole currentHole;
    //private Board board;

    public void Initialize(Board _board, Hole _startHole)
    {
        //board = _board;
        currentHole = _startHole;
        normalColor = render.material.color;
    }

    //highlight
    public void StartHighlight()
    {
        render.material.color = Color.yellow;

        for (int i = 0; i < currentHole.ValidHoleMoves.Count; i++)
        {
            currentHole.ValidHoleMoves[i].emptyHole.StartHighlight();

        }
    }

    public void EndHightlight()
    {
        render.material.color = normalColor;

        for (int i = 0; i < currentHole.ValidHoleMoves.Count; i++)
        {
            currentHole.ValidHoleMoves[i].emptyHole.EndHighlight();
        }
    }

    //holding

    public bool CanHold ()
    {
        return currentHole.ValidHoleMoves.Count > 0; 
    }

    public void StartHolding()
    {
        startPos = transform.position;
        transform.position += Vector3.up * 0.5f;
    }

    public void ResetHolding()
    {
        transform.position = startPos;
    }

    public void DestroyPeg()
    {
        currentHole.BindPeg(null);
        Destroy(gameObject);
    }

    //Apply Move
    public int IsValidHole()
    {
        HoleData _holeData = null;

        //make a ray from the peg to the board
        RaycastHit hit = new RaycastHit();
        Ray ray = new Ray(transform.position + Vector3.up, -Vector3.up);
        if(Physics.Raycast(ray,out hit, 3f, holeLayer))
        //if a hole is found, check if is a valid move
        {
            print(hit.transform.gameObject.name);
            _holeData = hit.transform.gameObject.GetComponent<HoleData>();
            //print(_holeData);
        }

        if (_holeData == null)
            return -1;

        for (int i = 0; i < currentHole.ValidHoleMoves.Count; i++)
        {
            if(currentHole.ValidHoleMoves[i].emptyHole == _holeData.GetHole)
            {
                return i;
            }
        }
        return -1;
    }


    public void MakeMove(int _moveIndex)
    {
        HoleMove move = currentHole.ValidHoleMoves[_moveIndex];

        Hole oldHole = currentHole;

        currentHole = move.emptyHole;//set new hole
        currentHole.BindPeg(this);

        oldHole.RemoveValidHole(_moveIndex);

        Vector3 newPos = currentHole.holeRenderer.transform.position;
        newPos.y = transform.position.y - 0.5f;
        transform.position = newPos;

        MoveData data = new MoveData();
        data.startHole = oldHole.HoleCoords;
        data.endHole = currentHole.HoleCoords;
        data.deletedPegHole = move.adjacentHole.HoleCoords;

        SaveManager.Instance.AddMove(data);
    }

    //for load
    public void MovePeg(Hole _endHole)
    {
        transform.position = _endHole.holeRenderer.transform.position;
        currentHole = _endHole;
        currentHole.BindPeg(this);
    }
}
