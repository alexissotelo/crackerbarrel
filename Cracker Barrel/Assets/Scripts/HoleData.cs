﻿using UnityEngine;
using System.Collections;

public class HoleData : MonoBehaviour {

    private Hole hole;

    public void Initialize(Hole _hole)
    {
        hole = _hole;
    }

    public Hole GetHole { get { return hole; } }
}
