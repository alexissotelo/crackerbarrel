﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Board
{
    public delegate void GameEvent(int remainingPegs);
    public static event GameEvent OnGameOver;

    private int totalValidMoves;

    private List<Hole[]> holes;
    private List<PegCtrl> remainingPegs;

    //CREATION
    public Board(int _amountPegs)
    {
        remainingPegs = new List<PegCtrl>();

        holes = new List<Hole[]>();

        for (int line = _amountPegs; line > 0; line--)
        {
            Hole[] holeLine = new Hole[line];
            for (int y = 0; y < line; y++)
            {
                holeLine[y] = new Hole(this,
                    (_amountPegs - line), y,
                    _amountPegs - 1, line - 1);
            }
            holes.Add(holeLine);
        }

        //Build adjacencies
        for (int x = 0; x < holes.Count; x++)
        {
            int ySize = holes[x].Length;
            for (int y = 0; y < ySize; y++)
            {
                GetHole(x, y).BuildAdjacencies();
            }
        }

    }

    public void PerformMoveList(MoveList _moveList)
    {
        Hole firstEmpty = GetHole(_moveList.moves[0].endHole);
        RemovePeg(firstEmpty.Peg);

        for (int i = 0; i < _moveList.moves.Count; i++)
        {
            Hole startHole = GetHole(_moveList.moves[i].startHole);
            Hole endHole = GetHole(_moveList.moves[i].endHole);
            Hole adjacentHole = GetHole(_moveList.moves[i].deletedPegHole);

            startHole.Peg.MovePeg(endHole);

            startHole.BindPeg(null);
            RemovePeg(adjacentHole.Peg);
        }
    }

    //HELPERS
    public void AddPeg(PegCtrl _peg)
    {
        remainingPegs.Add(_peg);
    }
    public void RemoveRandomPeg()
    {
        PegCtrl toRemove = remainingPegs[Random.Range(0, remainingPegs.Count - 1)];
        RemovePeg(toRemove);
    }

    //removes pegs from the board, updates the valid moves
    public void RemovePeg(PegCtrl _peg)
    {
        remainingPegs.Remove(_peg);
        _peg.DestroyPeg();

        totalValidMoves = 0;
        for (int i = 0; i < holes.Count; i++)
        {
            for (int j = 0; j < holes[i].Length; j++)
            {
                Hole hole = GetHole(i, j);
                if (!hole.IsEmpty)
                {
                    hole.BuildValidMoves();
                    totalValidMoves += hole.ValidHoleMoves.Count;
                }
            }
        }

        if (totalValidMoves == 0 && OnGameOver != null)
            OnGameOver(remainingPegs.Count);
    }

    //ACCESORS
    public Hole GetHole(int x, int y)
    {
        return holes[x][y];
    }

    public Hole GetHole(tHole _holeCoord)
    {
        return holes[_holeCoord.x][_holeCoord.y];
    }


    public int TotalValidMoves { get { return totalValidMoves; } }
}