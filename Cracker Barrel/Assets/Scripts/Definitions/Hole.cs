﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//for saving to file
[System.Serializable]
public struct tHole
{
    public int x, y;
}

//for easier peg removal
public struct HoleMove
{
    public Hole emptyHole;
    public Hole adjacentHole;
}

public class Hole
{
    public Renderer holeRenderer;

    private PegCtrl peg;
    private Board board;

    private List<Hole> adjacentHoles;
    private List<HoleMove> validHoleMoves;

    private int x, y;
    private int maxX;

    //START UP
    public Hole(Board _board, int _x, int _y, int _maxX, int _maxY)
    {
        peg = null;

        board = _board;
        adjacentHoles = new List<Hole>();
        validHoleMoves = new List<HoleMove>();

        x = _x;
        y = _y;
        maxX = _maxX;
        //maxY = _maxY;
    }

    public void BuildAdjacencies()
    {
        int[] dirX = { 1, -1, 0, 0, 1, -1 };
        int[] dirY = { 0, 0, 1, -1, -1, 1 };

        for (int i = 0; i < 6; i++)
        {
            int indexX = x + dirX[i];
            int indexY = y + dirY[i];

            if (indexX < 0 || indexX > maxX || indexY < 0 || indexY > maxX - indexX)
                continue;

            //Debug.Log(indexX + "," + indexY);
            adjacentHoles.Add(board.GetHole(indexX, indexY));
        }
    }

    public void BuildValidMoves()
    {
        validHoleMoves.Clear();
        for (int i = 0; i < adjacentHoles.Count; i++)
        {
            Hole adjacent = adjacentHoles[i];
            if (adjacent.IsEmpty)
                continue;

            //get direction
            int indexX = x + (adjacent.x - x) * 2;
            int indexY = y + (adjacent.y - y) * 2;

            //check if valid index
            if (indexX < 0 || indexX > maxX || indexY < 0 || indexY > maxX - indexX)
                continue;

            Hole possibleEmptyHole = board.GetHole(indexX, indexY);
            if (possibleEmptyHole.IsEmpty)
            {
                HoleMove move;
                move.adjacentHole = adjacent;
                move.emptyHole = possibleEmptyHole;
                validHoleMoves.Add(move);
            }
        }
    }

    //VISUAL FUNCTIONS
    public void StartHighlight()
    {
        holeRenderer.material.color = Color.yellow;
    }
    public void EndHighlight()
    {
        holeRenderer.material.color = Color.gray;
    }

    //MOVES
    public void BindPeg(PegCtrl _peg)
    {
        peg = _peg;
    }

    public void RemoveValidHole(int index)
    {
        BindPeg(null);
        board.RemovePeg(validHoleMoves[index].adjacentHole.Peg);
    }

    //ACCESSORS
    public List<HoleMove> ValidHoleMoves
    {
        get
        {
            return validHoleMoves;
        }
    }

    public bool IsEmpty { get { return Peg == null; } }

    public tHole HoleCoords
    {
        get
        {
            tHole _hole;
            _hole.x = x;
            _hole.y = y;
            return _hole;
        }
    }

    public PegCtrl Peg
    {
        get
        {
            return peg;
        }
    }
}