﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public BoardGenerator boardGenerator;

    [Header("UI")]
    public GameObject gameoverPanel;
    public Text gameoverLabel;

    public GameObject difficultyPanel;

    public GameObject exitButton;
    public Button loadButton;

    [Header("TIMER")]
    public Text timerLabel;
    public float countdownSeconds = 180;
    private bool bRunTimer = false;
    private float startTime;

    void OnEnable()
    {
        Board.OnGameOver += GameOver;
    }

    void OnDisable()
    {
        Board.OnGameOver -= GameOver;
    }

    void Start()
    {
        //check for save file
        loadButton.interactable = (SaveManager.Instance.MoveCount > 0);
    }

    void GameOver(int nRemainingPegs)
    {
        gameoverPanel.SetActive(true);
        gameoverLabel.text = nRemainingPegs.ToString();

        bRunTimer = false;
    }

    //UI funcs
    public void StartGame(int nSidePegs)
    {
        SaveManager.Instance.RestartMoveList();

        difficultyPanel.SetActive(false);
        //--
        boardGenerator.Generate(nSidePegs);
        //--

        bRunTimer = true;
        startTime = Time.time;

        exitButton.SetActive(true);
    }

    public void LoadGame()
    {
        difficultyPanel.SetActive(false);

        boardGenerator.Load();

        bRunTimer = true;
        startTime = Time.time;

        exitButton.SetActive(true);
    }

    public void UIReplay()
    {
        SceneManager.LoadScene(0);
    }


    public void UIUndo()
    {

    }
    public void UIRedo()
    {

    }

    void Update()
    {
        if (bRunTimer)
        {
            //display the timer
            float restSeconds = countdownSeconds - (Time.time - startTime);
            int roundedRestSeconds = Mathf.CeilToInt(restSeconds);

            int displaySeconds = roundedRestSeconds % 60;
            int displayMinutes = roundedRestSeconds / 60;
            timerLabel.text = string.Format("{0:0}:{1:00}", displayMinutes, displaySeconds);

            if (roundedRestSeconds <= 0)
            {
                GameOver(-1);
            }
        }
    }

}

