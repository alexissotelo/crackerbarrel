﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class BoardGenerator : MonoBehaviour
{
    public GameObject pegPrefab;
    public GameObject holePrefab;

    public Transform boardT;
    public Transform[] boardCornersT;

    [Header("Board Settings")]
    public uint amountPegs;//amount of pegs for each triangle side

    private Board board;

    public void Generate(int _amountPegs)
    {
        CreateBoard(_amountPegs);

        //Remove Peg
        board.RemoveRandomPeg();

        SaveManager.Instance.moveList.amountPegs = _amountPegs;
    }

    public void Load()
    {
        CreateBoard(SaveManager.Instance.moveList.amountPegs);

        board.PerformMoveList(SaveManager.Instance.moveList);

    }

    private void CreateBoard(int _amountPegs)
    {
        amountPegs = (uint)_amountPegs;
        board = new Board(_amountPegs);

        //triangle 0-1-2
        //==1==
        //=0=2=
        for (uint line = amountPegs; line > 0; line--)
        {
            uint x = (amountPegs - line);
            Vector3 spawnPosition = boardCornersT[0].position;//origin

            float xRatio = (float)x / (amountPegs - 1);
            spawnPosition += (boardCornersT[2].position - boardCornersT[0].position) * xRatio;

            for (int y = 0; y < line; y++)
            {
                if (y > 0)
                    spawnPosition += (boardCornersT[1].position - boardCornersT[0].position) * (1f / (amountPegs - 1));

                //=================
                //create peg
                GameObject peg = Instantiate(pegPrefab, spawnPosition, Quaternion.identity) as GameObject;
                peg.name = "peg " + (x + y);
                peg.transform.SetParent(boardT);
                PegCtrl pegCtrl = peg.GetComponent<PegCtrl>();

                //create hole
                GameObject hole = Instantiate(holePrefab, spawnPosition + Vector3.up * 0.3f, Quaternion.identity) as GameObject;
                hole.name = "hole " + x + "," + y;
                hole.transform.SetParent(boardT);

                Hole bindHole = board.GetHole((int)x, y);
                bindHole.holeRenderer = hole.GetComponent<Renderer>();
                bindHole.BindPeg(pegCtrl);

                //store new hole in HoleData
                HoleData data = hole.GetComponent<HoleData>();
                data.Initialize(bindHole);

                //bind peg to hole
                pegCtrl.Initialize(board, bindHole);

                //add to remaining pegs
                board.AddPeg(pegCtrl);
            }
        }
    }

}
